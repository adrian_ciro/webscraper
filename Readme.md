#README

Create console web scraper (http://en.wikipedia.org/wiki/Web_scraping) utility which:
 
Accepts as command line parameters:
 
 
-(must) web resources URL or path plain text containing a list of URLs

-(must) data command(s)

-(must) word (or list of words with “,” delimiter)

-(nice to have) output verbosity flag,  if on then the output should contains information about time spend on data scraping and data processing (-v

-(nice to have) extract sentences’ which contain given words (-e)
 
 
Supports the following data processing commands:
 
(must) count number of provided word(s) occurrence on webpage(s). (-w)

(must) count number of characters of each web page (-c)

(nice to have) extract sentences’ which contain given words (-e)

 
Data processing results should be printed to output for each web resources separately and for all resources as total.
 
Command line parameters example for Java implementation:

`java –jar scraper.jar http://www.cnn.com Greece,default –v –w –c –e`
 
Tips:
 - Do not use 3rd party libraries
 - Task fullfilment preferable programming languages are Java, C++/C#/C



##Implemented Solution

The scraping application is working with JAVA 8, using a MVC pattern composed by model, service and controller folders.
When the application is started, the MainController is responsible for initialize services, which are the ones
responsible for validate and process the data and manage the exceptions.
The services have interfaces showing the behavior and the corresponding implementation are used by MainController or
other services.
The MainController use the new features of concurrency like Executor Service and CompletableFuture in order to create an
independent process for each URL given.



##How do you start the application?

Execute in console or bash, inside the folder

`gradle assemble`

Then, in folder build/libs you will find the file scraper-1.0.jar


There are 2 scenarios for executing the app

A. Execute with only one url

`java -jar scraper-1.0.jar http://www.google.com google,main –v –w –c –e
`

B. Execute giving a file with a list of URLS, see in the resources folder an example "sample.txt"
This file MUST BE LOCATED in Desktop folder for both, Windows or Linux environments

`java -jar scraper-1.0.jar sample2.txt google,linkedin,main -v -w -c
`

NOTE: This app only works under JAVA 8
