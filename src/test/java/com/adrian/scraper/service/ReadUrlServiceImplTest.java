package com.adrian.scraper.service;

import com.adrian.scraper.model.CustomGenericException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Adrian
 */
@RunWith(MockitoJUnitRunner.class)
public class ReadUrlServiceImplTest {

    IReadUrlService readService;

    @Before
    public void setUp() throws Exception {
        readService = new ReadUrlServiceImpl();
    }

    @Test(expected = CustomGenericException.class)
    public void testReadResourceThrowsCustomGenericException() throws Exception {
        readService.readResource("RandomURL");
    }

    @Test
    public void testReadResourceReturnNotNull() throws Exception {
        List<String> result = readService.readResource("http://www.google.com/");
        assertNotNull(result);
    }

}