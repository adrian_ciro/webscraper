package com.adrian.scraper.service;

import com.adrian.scraper.model.Page;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Created by Adrian
 */
@RunWith(MockitoJUnitRunner.class)
public class ProcessDataServiceImplTest {

    @Mock
    IReadUrlService readUrlService;
    @Mock
    ICalculateTimeService calculateTimeService;
    @Mock
    IPrintService printService;
    @Mock
    Page page;

    @InjectMocks
    ProcessDataServiceImpl processDataService;


    @Before
    public void setUp() throws Exception {
        when(page.getUrl()).thenReturn("ULR");
        when(readUrlService.readResource(anyString())).thenReturn(Arrays.asList("Html","body", "Footer"));
        when(calculateTimeService.getTimeBetween(any(LocalTime.class),any(LocalTime.class))).thenReturn(LocalTime.parse("00:02"));
    }

    @Test(expected = AssertionError.class)
    public void testFetchContentUrlWithNullParameter() throws Exception {
        processDataService.fetchContentUrl(null);
    }

    @Test
    public void testFetchContentUrlWithNotNullParameter() throws Exception {
        processDataService.fetchContentUrl(page);

        verify(page).getUrl();
        verify(readUrlService).readResource(anyString());
        verify(calculateTimeService).getTimeBetween(any(LocalTime.class),any(LocalTime.class));
    }

    @Test(expected = AssertionError.class)
    public void testGetWordOccurrencesWithNullParameter() throws Exception {
        processDataService.getWordOccurrences(null);
    }

    @Test
    public void testGetWordOccurrencesWithNotNullParameter() throws Exception {
        List<String> words = new ArrayList<String>();
        words.add("word1");
        words.add("word2");
        Page page1 = new Page("URL", words);
        page1.setPageContent(Arrays.asList("Html word1","body word2asfads", "Footer word2"));
        processDataService.getWordOccurrences(page1);
        assertEquals(page1.getWords().get(0).getWordOcurrences(), 1);
        assertEquals(page1.getWords().get(1).getWordOcurrences(), 2);

    }

    @Test(expected = AssertionError.class)
    public void testGetNumberOfCharactersWithNullParameter() throws Exception {
        processDataService.getNumberOfCharacters(null);
    }

    @Test
    public void testGetNumberOfCharactersWithNotNullParameter() throws Exception {
        List<String> words = new ArrayList<String>();
        words.add("word1");
        Page page1 = new Page("URL", words);
        List<String> content = Arrays.asList("Html","body", "Footer");
        page1.setPageContent(content);

        processDataService.getNumberOfCharacters(page1);
        assertEquals(page1.getNumberOfCharacters(), 14);
    }


    @Test(expected = AssertionError.class)
    public void buildGeneralInfo() throws Exception {
        processDataService.buildGeneralInfo(null);
    }

}