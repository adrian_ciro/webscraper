package com.adrian.scraper.service;

import com.adrian.scraper.model.InputCommand;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Adrian
 */
@RunWith(MockitoJUnitRunner.class)
public class CommandServiceImplTest {

    @InjectMocks
    CommandServiceImpl commandService;
    @Mock
    IReadFileService readFileService;
    List<String> properParameters;
    List<String> invalidParameters;
    List<String> invalidParameters2;

    @Before
    public void setUp() throws Exception {
        properParameters = Arrays.asList("http://www.gmail.com", "google,mail", "-w", "-v");
        invalidParameters = Arrays.asList("wrongFileName", "google,mail", "-w", "-v");
        invalidParameters2 = Arrays.asList("wrongFileName", "google,mail");
    }

    @Test
    public void testReadArgumentsWithProperParameters() throws Exception {
        commandService.initializeServices();
        InputCommand result = commandService.readArguments((String[]) properParameters.toArray());

        assertNotNull(result);
    }


    @Test
    public void testReadArgumentsWithInvalidParameters() throws Exception {
        commandService.initializeServices();
        InputCommand result = commandService.readArguments((String[]) invalidParameters.toArray());

        assertNull(result);
    }

    @Test
    public void testReadArgumentsWithInvalidParameters2() throws Exception {
        commandService = mock(CommandServiceImpl.class);
        commandService.setReadFileService(readFileService);
        when(readFileService.getPath(Mockito.anyString())).thenReturn(Paths.get("source"));
        when(readFileService.readResource(Mockito.any(Path.class))).thenReturn(Arrays.asList("Html","body", "Footer"));
        commandService.urls = new ArrayList<String>();
        commandService.words = new ArrayList<String>();
        InputCommand result = commandService.readArguments((String[]) invalidParameters2.toArray());

        assertNull(result);
    }



}