package com.adrian.scraper.service;

import com.adrian.scraper.model.CustomGenericException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Created by Adrian
 */
@RunWith(MockitoJUnitRunner.class)
public class ReadFileServiceImplTest {

    IReadFileService readService;

    @Before
    public void setUp() throws Exception {
        readService = new ReadFileServiceImpl();
    }

    @Test(expected = CustomGenericException.class)
    public void testReadResourceThrowsCustomGenericException() throws Exception {
        readService.readResource(readService.getPath("RandomFile"));
    }

}