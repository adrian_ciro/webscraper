package com.adrian.scraper.service;

import com.adrian.scraper.model.InputCommand;
import com.adrian.scraper.model.Page;
import com.adrian.scraper.model.Word;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Created by Adrian
 */
@RunWith(MockitoJUnitRunner.class)
public class PrintServiceImplTest {

    @Mock
    Page page;
    @Mock
    Word word;

    IPrintService printService;

    @Before
    public void setUp() throws Exception {

        printService = new PrintServiceImpl();
        when(page.getUrl()).thenReturn("URL");
        when(page.getTimeOfScraping()).thenReturn(LocalTime.parse("00:00:50"));
        when(page.getTimeOfProcessingData()).thenReturn(LocalTime.parse("00:01:50"));
        when(page.getNumberOfCharacters()).thenReturn(10);
        when(page.getNumberOfCharacters()).thenReturn(10);
        when(page.hasError()).thenReturn(false);
        when(word.getWord()).thenReturn("word");
        when(word.getWordOcurrences()).thenReturn(5);
    }

    @Test
    public void printGeneralInfo() throws Exception {
        LocalTime initialTime = LocalTime.parse("10:00:00");
        LocalTime endTime = LocalTime.parse("10:00:02");
        printService.printGeneralInfo(initialTime, endTime, 5);
    }

    @Test
    public void testPrintInfoPerPageWhenPageHasNoErrorsAndFlagVIsON() throws Exception {
        InputCommand.verbosityFlagOn = true;
        printService.printInfoPerPage(page);
        verify(page).getTimeOfProcessingData();
        verify(page).getTimeOfScraping();
    }

    @Test
    public void testPrintInfoPerPageWhenPageHasNoErrorsAndFlagWIsON() throws Exception {
        InputCommand.occurrenceWordCounterFlagOn = true;
        List<Word> words = new ArrayList<Word>();
        words.add(word);
        when(page.getWords()).thenReturn(words);

        printService.printInfoPerPage(page);
        verify(page, atLeastOnce()).getWords();
        verify(word, atLeastOnce()).getWord();
        verify(word, atLeastOnce()).getWordOcurrences();
    }

    @Test
    public void testPrintInfoPerPageWhenPageHasNoErrorsAndFlagCIsON() throws Exception {
        InputCommand.characterCounterFlagOn = true;
        printService.printInfoPerPage(page);
        verify(page).getNumberOfCharacters();
    }

    @Test
    public void testPrintInfoPerPageWhenPageHasErrors() throws Exception {
        when(page.hasError()).thenReturn(true);
        printService.printInfoPerPage(page);
        verify(page).hasError();
    }

}