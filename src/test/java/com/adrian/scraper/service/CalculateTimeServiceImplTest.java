package com.adrian.scraper.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Adrian
 */
@RunWith(MockitoJUnitRunner.class)
public class CalculateTimeServiceImplTest {

    ICalculateTimeService calculateTimeService;
    List<LocalTime> times;

    @Before
    public void setUp() throws Exception {
        calculateTimeService = new CalculateTimeServiceImpl();
        times = new ArrayList<LocalTime>();
        times.add(LocalTime.parse("00:00:10"));
        times.add(LocalTime.parse("00:00:20"));
        times.add(LocalTime.parse("00:00:30"));
    }

    @Test(expected=AssertionError.class)
    public void testGetTimeBetweenFirstParameterNull() throws Exception {
        LocalTime initialTime = null;
        LocalTime endTime = LocalTime.now();

        LocalTime result = calculateTimeService.getTimeBetween(initialTime, endTime);
        assertTrue(result == null);
    }

    @Test
    public void testGetTimeBetweenParametersAreNotNull() throws Exception {
        LocalTime initialTime = LocalTime.parse("10:00:00");
        LocalTime endTime = LocalTime.parse("10:00:02");

        LocalTime result = calculateTimeService.getTimeBetween(initialTime, endTime);
        assertEquals(result.toString(),("00:00:02"));
    }

    @Test
    public void testSumUpTimesWithAllParametersValid() throws Exception {
        LocalTime result = calculateTimeService.SumUpTimes(times);
        assertEquals(result.toString(), ("00:01"));
    }

    @Test(expected=NullPointerException.class)
    public void testSumUpTimesWithNullParameters() throws Exception {
        times.add(null);
        LocalTime result = calculateTimeService.SumUpTimes(times);
    }

}