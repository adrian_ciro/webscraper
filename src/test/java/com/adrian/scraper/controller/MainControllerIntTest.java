package com.adrian.scraper.controller;

import com.adrian.scraper.model.InputCommand;
import com.adrian.scraper.model.Page;
import com.adrian.scraper.service.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

/**
 * Created by Adrian
 */
@RunWith(MockitoJUnitRunner.class)
public class MainControllerIntTest {

    String[] args1 = {"http://www.google.ee", "hi,google","-v"};
    String[] args2 = {"http://www.google.ee", "google,other","-w"};
    String[] args3 = {"http://www.google.ee", "google","-c"};
    String[] args4 = {"http://www.google.ee", "google","-e"};
    String[] args5 = {"http://www.linkedin.com", "linkedin,google,other","-v", "-w", "-c", "-e"};

    MainController mainController;

    @Test
    public void testMainControllerWithFlagV() throws Exception {
        mainController = new MainController(args1);
    }

    @Test
    public void testMainControllerWithFlagW() throws Exception {
        mainController = new MainController(args2);
    }

    @Test
    public void testMainControllerWithFlagC() throws Exception {
        mainController = new MainController(args3);
    }

    @Test
    public void testMainControllerWithFlagE() throws Exception {
        mainController = new MainController(args4);
    }

    @Test
    public void testMainControllerWithAllFlags() throws Exception {
        mainController = new MainController(args5);
    }


}