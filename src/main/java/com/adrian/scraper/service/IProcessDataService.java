package com.adrian.scraper.service;

import com.adrian.scraper.model.Page;

import java.util.List;

/**
 * Interface for providing services related to processing the data according to the flags -v -w -c -e
 * Created by Adrian
 */
public interface IProcessDataService extends IService{

    void fetchContentUrl(Page page);
    void fetchSentencesFromContent(Page page);
    void getWordOccurrences(Page page);
    void getNumberOfCharacters(Page page);
    void buildGeneralInfo(List<Page> pages);
}
