package com.adrian.scraper.service;

import java.util.List;

/**
 * Created by Adrian
 */
public interface IReadUrlService {
    List<String> readResource(String path);
}
