package com.adrian.scraper.service;

import com.adrian.scraper.model.InputCommand;
import com.adrian.scraper.model.Page;

import java.time.LocalTime;

/**
 * Implementation of the Interface @IPrintService
 * Created by Adrian
 */
public class PrintServiceImpl implements IPrintService {

    /**
     * Synchronized method used to show the info attached to a specific Page
     * @param page
     */
    @Override
    public synchronized void printInfoPerPage(Page page) {
        if(!page.hasError()){

            printMsg("--------------------------------");
            printMsg("INFO ABOUT PAGE " + page.getUrl());

            if(InputCommand.verbosityFlagOn){
                printMsg("Time spend on data scraping: " + page.getTimeOfScraping());
                printMsg("Time spend on data processing: " + page.getTimeOfProcessingData());
            }

            if(InputCommand.occurrenceWordCounterFlagOn){
                page.getWords().stream().forEach(word -> printMsg("Occurrences of the word " +
                        word.getWord() + ": " + word.getWordOcurrences()));
            }

            if(InputCommand.characterCounterFlagOn){
                printMsg("Number of characters on url: " + page.getNumberOfCharacters());
            }

            if(InputCommand.sentenceFlagOn){
                page.getWords().stream().forEach(word -> {
                    printMsg("Sentences of the word " + word.getWord() + ": ");
                    word.getSentencesWithWordFound().stream().forEach(s -> printMsg(s));
                });
            }

            printMsg("--------------------------------" + "\n\n");
        }
        else{
            printMsg("Unfortunately the content for URL: "+ page.getUrl() + " was empty. " +
                    "This can happen if the timeout for fetching was reached, the access to the page is forbidden, " +
                    "url does not use HTTPS protocol, etc. \n\n");
        }
    }

    /**
     * Method used to print the complete info of the scrapping pages if InputCommand.verbosityFlagOn is true
     * @param totalTimeOfScraping
     * @param totalTimeOfProcessing
     * @param numberOfURLs
     */
    @Override
    public void printGeneralInfo(LocalTime totalTimeOfScraping, LocalTime totalTimeOfProcessing, long numberOfURLs) {
        printMsg("*******************************");
        printMsg("TOTAL TIME FOR SCRAPING " + numberOfURLs + " URLS: " + totalTimeOfScraping);
        printMsg("TOTAL TIME FOR PROCESSING " + numberOfURLs + " URLS: " + totalTimeOfProcessing);
        printMsg("*******************************");
    }

    /**
     * Short method to print a msg in console
     * @param msg
     */
    public static void printMsg(String msg){
        System.out.println(msg);
    }
}
