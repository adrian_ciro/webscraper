package com.adrian.scraper.service;

/**
 * Interface for providing services related to initiate other services
 * Created by Adrian
 */
public interface IService {

    void initializeServices();
}
