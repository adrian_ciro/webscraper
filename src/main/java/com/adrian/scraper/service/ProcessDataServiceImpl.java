package com.adrian.scraper.service;

import com.adrian.scraper.model.CustomGenericException;
import com.adrian.scraper.model.Page;

import java.time.LocalTime;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Implementation of the Interface @IProcessDataService
 * Created by Adrian
 */
public class ProcessDataServiceImpl implements IProcessDataService {

    private IReadUrlService readUrlService;
    private ICalculateTimeService calculateTimeService;
    private IPrintService printService;

    /**
     * Initiate services of the class
     */
    @Override
    public void initializeServices() {
        readUrlService = new ReadUrlServiceImpl();
        calculateTimeService = new CalculateTimeServiceImpl();
        printService = new PrintServiceImpl();
    }

    /**
     * Synchronized method used to read the url inside page
     * @param page
     */
    @Override
    public synchronized void fetchContentUrl(Page page) {
        assert(page != null);

        try{
            LocalTime startTimeOfScraping = LocalTime.now();
            List<String> content =  readUrlService.readResource(page.getUrl());
            if(content != null)
                page.setPageContent(content);
            else
                page.setHasError(true);

            LocalTime endTimeOfScraping = LocalTime.now();
            page.setTimeOfScraping(calculateTimeService.getTimeBetween(startTimeOfScraping, endTimeOfScraping));

        }catch (CustomGenericException c){
            c.printException();
            page.setHasError(true);
        }catch (Exception c){
            c.printStackTrace();
            page.setHasError(true);
        }
    }

    /**
     * Synchronized method used to find the occurrences of all the words given
     * @param page
     */
    @Override
    public synchronized void getWordOccurrences(Page page) {
        assert(page != null);
        page.getWords().forEach(w-> {
            page.getPageContent().stream().forEach(s->w.setWordOcurrences(w.getWordOcurrences() + s.split(w.getWord(),  -1).length -1));
        });
    }

    /**
     * Synchronized method used to get the total number of characters on the content page
     * @param page
     */
    @Override
    public synchronized void getNumberOfCharacters(Page page) {
        assert(page != null);
        page.getPageContent().stream().forEach(s->page.setNumberOfCharacters(page.getNumberOfCharacters() + s.length()));
    }

    /**
     * Synchronized method used to set inside each word the sentences found according to the words given
     * @param page
     */
    @Override
    public synchronized void fetchSentencesFromContent(Page page) {
        assert(page != null);
        page.getWords().forEach(w-> {w.getSentencesWithWordFound().addAll(getSentences(page.getPageContent().toString(), w.getWord()));});
    }

    /**
     * Method used to build the general result once all the pages have finished their execution
     * @param pages
     */
    @Override
    public void buildGeneralInfo(List<Page> pages) {
        assert(pages != null);
        LocalTime totalTimeForScraping = calculateTimeService.SumUpTimes(pages.stream().filter(page -> !page.hasError()).map(Page::getTimeOfScraping).collect(Collectors.toList()));
        LocalTime totalTimeForProcessing = calculateTimeService.SumUpTimes(pages.stream().filter(page -> !page.hasError()).map(Page::getTimeOfProcessingData).collect(Collectors.toList()));

        printService.printGeneralInfo(totalTimeForScraping, totalTimeForProcessing, pages.stream().filter(page -> !page.hasError()).count());
    }

    /**
     * Method used in @fetchSentencesFromContent which iterate over @content looking for sentences containing @wordToFind
     * @param content String
     * @param wordToFind String
     * @return List of String
     */
    private List<String> getSentences(String content, String wordToFind) {
        assert(content != null && wordToFind != null);
        return Stream.of(content.split("\\.")).filter(s -> s.contains(wordToFind)).collect(Collectors.toList());
    }

}
