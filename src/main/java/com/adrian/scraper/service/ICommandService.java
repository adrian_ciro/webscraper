package com.adrian.scraper.service;

import com.adrian.scraper.model.InputCommand;

/**
 * Interface for providing services related to read and verify the command line parameters
 * Created by Adrian
 */
public interface ICommandService extends IService{

    InputCommand readArguments(String[] args);

}
