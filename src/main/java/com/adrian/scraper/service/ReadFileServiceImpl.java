package com.adrian.scraper.service;

import com.adrian.scraper.model.CustomGenericException;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static com.adrian.scraper.model.ErrorCode.ERROR_101;

/**
 * Implementation of the Interface @IReadFileService, to specifically read a file from System
 * Created by Adrian
 */
public class ReadFileServiceImpl implements IReadFileService {

    public static final String USER_HOME = "user.home";
    public static final String FOLDER_NAME_DESKTOP = "Desktop";

    @Override
    public List<String> readResource(Path path) {
        assert (path != null);
        List<String> urls = new ArrayList<String>();
        try (Stream<String> stream = Files.lines(path, Charset.defaultCharset())) {
            stream.forEach(urls::add);
        } catch (NoSuchFileException ex) {
           throw new CustomGenericException(ERROR_101.getErrorCode(), ERROR_101.getErrorMessage());
        }
        catch (Exception ex) {
           throw new CustomGenericException("0", ex.getMessage());
        }
        return urls;
    }

    @Override
    public Path getPath(String resource) {
        Path path= null;
        return Paths.get(System.getProperty(USER_HOME), FOLDER_NAME_DESKTOP, resource);
    }
}
