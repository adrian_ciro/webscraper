package com.adrian.scraper.service;

import com.adrian.scraper.model.Page;

import java.time.LocalTime;
import java.util.List;

/**
 * Interface for providing services related to printing
 * Created by Adrian
 */
public interface IPrintService {

    void printInfoPerPage(Page page);

    void printGeneralInfo(LocalTime totalTimeOfScraping, LocalTime totalTimeOfProcessing, long numberOfUrls);
}
