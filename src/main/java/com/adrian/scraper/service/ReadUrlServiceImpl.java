package com.adrian.scraper.service;

import com.adrian.scraper.model.CustomGenericException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import static com.adrian.scraper.model.ErrorCode.*;

/**
 * Implementation of the Interface @IReadFileService, to specifically read a URL resource from the web
 * Created by Adrian
 */
public class ReadUrlServiceImpl implements IReadUrlService {

    URL website;
    URLConnection conn;
    List<String> contentList;

    @Override
    public synchronized List<String> readResource(String resource) {

        contentList =  new ArrayList<String>();
        try {
            website = new URL(resource);
            conn = website.openConnection();
            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            reader.lines().forEach(s -> contentList.add(s));
        } catch (MalformedURLException e) {
            throw new CustomGenericException(ERROR_104.getErrorCode(), ERROR_104.getErrorMessage());
        } catch (UnsupportedEncodingException e) {
            throw new CustomGenericException(ERROR_105.getErrorCode(), ERROR_105.getErrorMessage());
        } catch (IOException e) {
            throw new CustomGenericException(ERROR_106.getErrorCode(), resource + " " + ERROR_106.getErrorMessage());
        } catch (Exception e) {
            throw new CustomGenericException("0", e.getMessage());
        }

        return contentList.isEmpty() ? null : contentList;
    }
}
