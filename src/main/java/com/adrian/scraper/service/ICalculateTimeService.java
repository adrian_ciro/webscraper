package com.adrian.scraper.service;

import java.time.LocalTime;
import java.util.List;

/**
 * Interface for providing services related to time calculations
 * Created by Adrian
 */
public interface ICalculateTimeService {

    LocalTime getTimeBetween(LocalTime initialTime, LocalTime finalTime);
    LocalTime SumUpTimes(List<LocalTime> times);

}
