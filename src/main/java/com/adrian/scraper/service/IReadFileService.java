package com.adrian.scraper.service;

import java.nio.file.Path;
import java.util.List;

/**
 * Interface for providing services related to reading Files or URLs resources
 * Created by Adrian
 */
public interface IReadFileService {

    List<String> readResource(Path path);
    Path getPath(String resource);
}
