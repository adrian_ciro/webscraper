package com.adrian.scraper.service;

import java.time.LocalTime;
import java.util.List;

import static java.time.temporal.ChronoUnit.NANOS;

/**
 * Implementation of the Interface @ICalculateTimeService
 * Created by Adrian
 */
public class CalculateTimeServiceImpl implements ICalculateTimeService {

    @Override
    public LocalTime getTimeBetween(LocalTime initialTime, LocalTime finalTime) {
        assert (initialTime != null && finalTime != null);
        return LocalTime.ofNanoOfDay(NANOS.between(initialTime, finalTime));
    }

    @Override
    public LocalTime SumUpTimes(List<LocalTime> times) {
        LocalTime totalTime = null;
        for(LocalTime time: times){
            if(totalTime == null)
                totalTime = time;
            else
                totalTime = totalTime.plusNanos(time.toNanoOfDay());
        }
        return totalTime;
    }
}
