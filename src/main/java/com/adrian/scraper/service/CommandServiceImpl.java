package com.adrian.scraper.service;

import com.adrian.scraper.model.InputCommand;
import com.adrian.scraper.model.CustomGenericException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.adrian.scraper.model.ErrorCode.ERROR_100;
import static com.adrian.scraper.model.ErrorCode.ERROR_102;

/**
 * Implementation of the Interface @ICommandService
 * Created by Adrian
 */
public class CommandServiceImpl implements ICommandService {

    final static String HTTP_PREFIX = "http://";
    final static String VERBOSITY_COMMAND = "-v";
    final static String OCCURRENCE_COMMAND = "-w";
    final static String CHARACTER_COMMAND = "-c";
    final static String SENTENCE_COMMAND = "-e";
    boolean URLRead;
    boolean wordsRead;
    List<String> urls;
    List<String> words;

    IReadFileService readFileService;

    /**
     * Initiate services and variables of the class
     */
    @Override
    public void initializeServices() {
        URLRead = false;
        wordsRead = false;
        urls = new ArrayList<String>();
        words = new ArrayList<String>();
        readFileService = new ReadFileServiceImpl();
    }

    @Override
    public InputCommand readArguments(String[] args) {

        try{
            Arrays.stream(args).forEach(s -> validateArgument(s));
            isAnyCommandPresent();
        }
        catch (CustomGenericException c){
            c.printException();
            return null;
        }

        return new InputCommand(urls, words);
    }

    /**
     * Validate that at least one input parameter was given
     * If no, throw a CustomGenericException
     */
    public void isAnyCommandPresent() {
        if(!(InputCommand.verbosityFlagOn || InputCommand.occurrenceWordCounterFlagOn || InputCommand.characterCounterFlagOn
                || InputCommand.sentenceFlagOn)){
            throw new CustomGenericException(ERROR_102.getErrorCode(), ERROR_102.getErrorMessage());
        }
    }

    /**
     * Validate that the arguments given are in the following order
     * 1st A url or a file
     * 2nd a string of words separated by coma
     * 3rd or more one of the following flags -v -w -e -c
     * If the above order is not correct, then throw a CustomGenericException
     * @param s String input parameter
     */
    public void validateArgument(String s) {

        if(s.contains(HTTP_PREFIX) && !URLRead){
            urls.add(s);
            URLRead = true;
            return;
        }

        if(!URLRead && urls.isEmpty()){
            urls = readFileService.readResource(readFileService.getPath(s));
            URLRead = true;
            return;
        }

        if(URLRead && words.isEmpty()){

            wordsRead = true;
            if(!s.contains(",")){
                words.add(s);
                return;
            }

            Arrays.stream(s.split(",")).forEach(s1 -> words.add(s1.trim()));
            return;
        }

        if(wordsRead){

            switch (s){
                case VERBOSITY_COMMAND: InputCommand.verbosityFlagOn = true; return;
                case OCCURRENCE_COMMAND: InputCommand.occurrenceWordCounterFlagOn = true; return;
                case CHARACTER_COMMAND: InputCommand.characterCounterFlagOn = true; return;
                case SENTENCE_COMMAND: InputCommand.sentenceFlagOn = true; return;
            }
        }

        throw new CustomGenericException(ERROR_100.getErrorCode(), ERROR_100.getErrorMessage());

    }

    public void setReadFileService(IReadFileService readFileService) {
        this.readFileService = readFileService;
    }

}
