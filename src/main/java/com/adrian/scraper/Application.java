package com.adrian.scraper;

import com.adrian.scraper.controller.MainController;

/**
 * Main class for running application
 * Created by Adrian
 */
public class Application  {

    public static void main(String[] args) {
        new MainController(args);
    }
}
