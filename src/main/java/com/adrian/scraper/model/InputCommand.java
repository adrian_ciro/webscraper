package com.adrian.scraper.model;

import java.util.List;

/**
 * Class to map the arguments given by the user to initiate the app
 * Created by Adrian
 */
public class InputCommand {

    private List<String> urls;
    private List<String> words;
    public static boolean verbosityFlagOn;
    public static boolean occurrenceWordCounterFlagOn;
    public static boolean characterCounterFlagOn;
    public static boolean sentenceFlagOn;

    public InputCommand(List<String> urls, List<String> words) {
        this.urls = urls;
        this.words = words;
    }

    public List<String> getUrls() {
        return urls;
    }

    public void setUrls(List<String> urls) {
        this.urls = urls;
    }

    public List<String> getWords() {
        return words;
    }

    public void setWords(List<String> words) {
        this.words = words;
    }
}
