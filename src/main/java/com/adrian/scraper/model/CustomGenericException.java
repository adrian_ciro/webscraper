package com.adrian.scraper.model;

/**
 * Custom generic exception maps the the basic info for custom exceptions of scraper
 * Created by Adrian
 */
public class CustomGenericException extends RuntimeException{

    private String errCode;
    private String errMsg;

    public CustomGenericException(String errCode, String errMsg) {
        this.errCode = errCode;
        this.errMsg = errMsg;
    }

    public String getErrCode() {
        return errCode;
    }

    public void setErrCode(String errCode) {
        this.errCode = errCode;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public synchronized void printException(){
        System.out.println("Exception found on the Web Scraper: " + "Error: " + errCode + ": " + errMsg);
    }


}
