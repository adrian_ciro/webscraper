package com.adrian.scraper.model;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Class to map all the info needed for each URL
 * Created by Adrian
 */
public class Page {

    private String url;
    private List<Word> words;
    private List<String> pageContent;
    private LocalTime timeOfScraping;
    private LocalTime timeOfProcessingData;
    private int numberOfCharacters;
    private boolean hasError;

    public Page(String url, List<String> words) {
        this.url = url;
        List<Word> wordList = new ArrayList<Word>();
        words.stream().forEach(w -> wordList.add(new Word(w)));
        this.words = wordList;
        this.numberOfCharacters = 0;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<Word> getWords() {
        return words;
    }

    public void setWords(List<Word> words) {
        this.words = words;
    }

    public List<String> getPageContent() {
        return pageContent;
    }

    public void setPageContent(List<String>  pageContent) {
        this.pageContent = pageContent;
    }

    public LocalTime getTimeOfScraping() {
        return timeOfScraping;
    }

    public void setTimeOfScraping(LocalTime timeOfScraping) {
        this.timeOfScraping = timeOfScraping;
    }

    public LocalTime getTimeOfProcessingData() {
        return timeOfProcessingData;
    }

    public void setTimeOfProcessingData(LocalTime timeOfProcessingData) {
        this.timeOfProcessingData = timeOfProcessingData;
    }

    public int getNumberOfCharacters() {
        return numberOfCharacters;
    }

    public void setNumberOfCharacters(int numberOfCharacters) {
        this.numberOfCharacters = numberOfCharacters;
    }

    public boolean hasError() {
        return hasError;
    }

    public void setHasError(boolean hasErrors) {
        this.hasError = hasErrors;
    }
}
