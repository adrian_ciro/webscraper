package com.adrian.scraper.model;

/**
 * Enum to define all the errors that can occur in the entire application
 * Created by Adrian
 */
public enum ErrorCode {

    ERROR_100("100", "Parameters are invalid for web scraping. Be sure it has the following pattern for correct execution:" +
            " java -jar scraperHR.jar  http://www.cnn.com Greece,default –v –w –c –e"),
    ERROR_101("101", "File no found. Please be sure the file with propers urls is located at your Desktop folder. Please try again"),
    ERROR_102("102", "Missing data processing commands. Please add one of the following options -v -w -e -c"),
    ERROR_103("103", "TimeOut reached for a url"),
    ERROR_104("104", "URL is Malformed. Please correct the URL(s)"),
    ERROR_105("105", "Encoding Error in URL. Please correct the URL(s)"),
    ERROR_106("106", "URL was accessed but not authorized for fetching content. Please try another URL(s)");

    private final String errorCode;
    private final String errorMessage;

    ErrorCode(String errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
