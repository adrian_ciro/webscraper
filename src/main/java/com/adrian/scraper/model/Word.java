package com.adrian.scraper.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Class to map specific info for each word given
 * Created by Adrian
 */
public class Word {

    private String word;
    private int wordOcurrences;
    private List<String> sentencesWithWordFound;

    public Word(String word) {
        this.word = word;
        this.wordOcurrences = 0;
        sentencesWithWordFound = new ArrayList<String>();
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public int getWordOcurrences() {
        return wordOcurrences;
    }

    public void setWordOcurrences(int wordOcurrences) {
        this.wordOcurrences = wordOcurrences;
    }

    public List<String> getSentencesWithWordFound() {
        return sentencesWithWordFound;
    }

    public void setSentencesWithWordFound(List<String> sentencesWithWordFound) {
        this.sentencesWithWordFound = sentencesWithWordFound;
    }

}
