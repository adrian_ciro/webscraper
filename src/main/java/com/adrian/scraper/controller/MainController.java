package com.adrian.scraper.controller;

import com.adrian.scraper.model.InputCommand;
import com.adrian.scraper.model.CustomGenericException;
import com.adrian.scraper.model.Page;
import com.adrian.scraper.service.*;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.concurrent.*;

import static com.adrian.scraper.model.ErrorCode.ERROR_103;

/**
 * Main controller responsible for initialize other services, verify arguments, initialize the pages
 * and create a Thread pool which handles different CompletableFuture objects to scrap each url
 * Created by Adrian
 */
public class MainController {

    public static final int TIMEOUT_IN_SECONDS = 10;
    public static final int SLEEPTIME_IN_MILLISECONDS = 5000;

    private InputCommand inputCommand;
    private ICommandService commandService;
    private static ICalculateTimeService calculateTimeService;
    private static IProcessDataService processDataService;
    private static IPrintService printService;
    static List<Page> pages;

    public MainController() {
    }

    /**
     * Constructor in order to initiate the whole process
     * @param args
     */
    public MainController(String[] args) {
        initializeServices();
        inputCommand = commandService.readArguments(args);
        if(inputCommand != null){
            startScrapingProcess();
        }
    }

    /**
     * Initialize services and variables
     */
    public void initializeServices() {
        commandService = new CommandServiceImpl();
        commandService.initializeServices();
        processDataService = new ProcessDataServiceImpl();
        processDataService.initializeServices();
        printService = new PrintServiceImpl();
        calculateTimeService = new CalculateTimeServiceImpl();
        pages = new ArrayList<Page>();
    }

    /**
     * Initiate scraping process through 4 steps
     * Create the completableFutures, Call the futures and Set the timeout for all of them,
     * Wait until all futures are complete, Finish calculations in case of -v
     */
    public void startScrapingProcess(){
        ExecutorService executor = Executors.newWorkStealingPool();     //Pool of all cpu's available for Threading
        List<CompletableFuture<Page>> futureList = new ArrayList<CompletableFuture<Page>>();

        //STEP 1: Create the completableFutures
        inputCommand.getUrls().stream().forEach(url-> futureList.add(addFuture(executor, url, inputCommand.getWords())));

        //STEP 2: Call the futures and Set the timeout for all of them
        futureList.stream().forEach(fut -> {
            try {
                fut.get(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                fut.completeExceptionally(new CustomGenericException("0", e.getMessage()));
            } catch (ExecutionException e) {
                fut.completeExceptionally(new CustomGenericException("1", e.getMessage()));
            } catch (TimeoutException e) {
                fut.completeExceptionally(new CustomGenericException(ERROR_103.getErrorCode(), ERROR_103.getErrorMessage()));
            }
        });

        //STEP 3: Wait until all futures are complete
        while(!isScrapingCompleteForAllFutures(futureList)){
            try {
                Thread.sleep(SLEEPTIME_IN_MILLISECONDS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        //STEP 4: Finish calculations in case of InputCommand -v and printout
        if (InputCommand.verbosityFlagOn) {
            processDataService.buildGeneralInfo(pages);
        }

    }

    /**
     * Creation of the CompletableFuture and adding it to executorService
     * Checks all the flags on and process them eg -w -v -e -c
     * @param ex ExecutorService
     * @param url String resource
     * @param words List of words to search for
     * @return
     */
    public static CompletableFuture<Page> addFuture(ExecutorService ex, String url, List<String> words){
        CompletableFuture<Page> pageProcessor = CompletableFuture.supplyAsync(() -> {
            Page page = null;
            LocalTime startDataProcessing = null;
            LocalTime endDataProcessing = null;
            try {
                page = new Page(url, words);
                processDataService.fetchContentUrl(page);

                if(page.getPageContent()!=null) {
                    startDataProcessing = LocalTime.now();

                    if (InputCommand.occurrenceWordCounterFlagOn) { //Flag -w
                        processDataService.getWordOccurrences(page);
                    }

                    if (InputCommand.characterCounterFlagOn) { //Flag -c
                        processDataService.getNumberOfCharacters(page);
                    }

                    if (InputCommand.sentenceFlagOn) { //Flag -e
                        processDataService.fetchSentencesFromContent(page);
                    }

                    endDataProcessing = LocalTime.now();
                    page.setTimeOfProcessingData(calculateTimeService.getTimeBetween(startDataProcessing, endDataProcessing));

                }

                printService.printInfoPerPage(page);
                pages.add(page);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return page;
        });
        return pageProcessor;
    }

    /**
     * Validate if all Completable future have finished their execution
     * @param futureList
     * @return
     */
    public boolean isScrapingCompleteForAllFutures(List<CompletableFuture<Page>> futureList){
        return futureList.stream().allMatch(f-> f.isDone() || f.isCompletedExceptionally());
    }

}
